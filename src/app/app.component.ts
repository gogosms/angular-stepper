import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { RegistrationStep1Component } from './components/registration-step1/registration-step1.component';
import { RegistrationStep2Component } from './components/registration-step2/registration-step2.component';
import { RegistrationStep3Component } from './components/registration-step3/registration-step3.component';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class AppComponent implements OnInit {
   title = 'Angular Material Stepper';
    @ViewChild('StepOneComponent') stepOneComponent: RegistrationStep1Component;
    @ViewChild('StepTwoComponent') stepTwoComponent: RegistrationStep2Component;
    constructor() { }
    ngOnInit(): void {
    }

  get frmStepOne() {
    if (this.stepOneComponent && this.stepOneComponent.frmStepOne) {
      return true;
    }
    return false;
 }

  get frmStepTwo() {
    if (this.stepTwoComponent && this.stepTwoComponent.frmStepTwo) {
      return true;
    }
    return false;
 }

}
