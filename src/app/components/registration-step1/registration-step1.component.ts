import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration-step1',
  templateUrl: './registration-step1.component.html',
  styleUrls: ['./registration-step1.component.css']
})
export class RegistrationStep1Component implements OnInit {

  constructor(private _formBuilder: FormBuilder) {
    this.frmStepOne = this._formBuilder.group({});
   }

  public frmStepOne: FormGroup;

  ngOnInit() {
    this.frmStepOne = this._formBuilder.group({
      firstname: [null, Validators.required],
      mi: [null] ,
      lastname : [null, Validators.required]});
  }

  step1Submitted() {
    this.frmStepOne.get('firstname').markAsTouched();
    this.frmStepOne.get('firstname').updateValueAndValidity();
    this.frmStepOne.get('lastname').markAsTouched();
    this.frmStepOne.get('lastname').updateValueAndValidity();
  }

}
