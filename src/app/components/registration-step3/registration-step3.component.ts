import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-registration-step3',
  templateUrl: './registration-step3.component.html',
  styleUrls: ['./registration-step3.component.css']
})
export class RegistrationStep3Component implements OnInit {

  @Input() frmStepOne: FormGroup;
  @Input() frmStepTwo: FormGroup;

  constructor() { }
  ngOnInit() {
  }

  submit() {
    console.log('Step1: ' + JSON.stringify(this.frmStepOne.getRawValue()));
    console.log('Step2: ' + JSON.stringify(this.frmStepTwo.getRawValue()));
  }

}
