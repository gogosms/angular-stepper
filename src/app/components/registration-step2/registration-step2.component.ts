import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration-step2',
  templateUrl: './registration-step2.component.html',
  styleUrls: ['./registration-step2.component.css']
})
export class RegistrationStep2Component implements OnInit {

  constructor(private _formBuilder: FormBuilder) {
    this.frmStepTwo = this._formBuilder.group({});
   }

  frmStepTwo: FormGroup;

  ngOnInit() {
    this.frmStepTwo = this._formBuilder.group({
      email: [null, Validators.email],
      phone: [null]
    });
  }

  step2Submitted() {
    this.frmStepTwo.get('email').markAsTouched();
    this.frmStepTwo.get('email').updateValueAndValidity();
  }

}
